import classNames from "classnames";
import * as React from "react";

export type BtnProps = React.HTMLAttributes<HTMLButtonElement> & {};

export const Btn: React.FC<BtnProps> = (props) => {
  const { children, className, ...rest } = props;
  return (
    <button className={classNames('btn', className)} {...rest}>
      {children}
    </button>
  );
};