import * as React from "react";
import {Btn, BtnProps } from "./Btn";

export const PrimaryBtn: React.FC<BtnProps> = (props) => {
  const { ...rest } = props;
  return (
    <Btn className="btn-primary" {...rest} />
  );
};