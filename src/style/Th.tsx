import * as React from "react";
import styles from "../table.module.scss";
import { Cell, CellProps } from "./Cell";

export type ThProps = CellProps;

export const Th: React.FC<ThProps> = (props) => {
  return <Cell className={styles.th} {...props} />;
};
