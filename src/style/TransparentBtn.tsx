import * as React from "react";
import {Btn, BtnProps } from "./Btn";

export const TransparentBtn: React.FC<BtnProps> = (props) => {
  const { ...rest } = props;
  return (
    <Btn className="btn-transparent" {...rest} />
  );
};