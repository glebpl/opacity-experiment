import React, { useRef, useCallback, useState } from 'react';
import {throttle, toggleClass } from '../utils';

interface Props extends React.HTMLAttributes<HTMLDivElement>{
  initialLeft: number;
  onDragMove(left: number): void;
}

const VerticalDelimiter: React.FC<Props> = props => {

  const {
    initialLeft,
    onDragMove,
    ...rest
  } = props;

  // React.useEffect(() => {
  //   if(ref.current) {
  //     const $div = $(ref.current);
  //     $div.auiSelect2();
  //   }
  // }, []);

  const ref = useRef<HTMLDivElement>(null);
  const [left, setLeft] = useState(initialLeft);

  const handleDragStart = useCallback(e => {
    if(ref.current) {
      toggleClass(ref.current, 'dragged', true);
    }
  }, []);

  const handleDragEnd = useCallback(e => {
    if(ref.current) {
      toggleClass(ref.current, 'dragged', false);
    }
  }, []);

  const handleDrag = useCallback(throttle((e: DragEvent) => {
    if(ref.current && e.clientX > 0) {
      setLeft(e.clientX);
      onDragMove(e.clientX);
    }
  }, 20), []);

  return (
    <div
      ref={ref}
      className="delimiter"
      draggable
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragOver={e => e.preventDefault()}
      onDrag={handleDrag}
      style={{
        left: `${left}px`
      }}
      {...rest}
    />
  );
};

export default VerticalDelimiter;
