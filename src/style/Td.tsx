import * as React from "react";
import styles from "../table.module.scss";
import { Cell, CellProps } from "./Cell";

export type TdProps = CellProps & {
  useOpacity: boolean;
};

export const Td: React.FC<TdProps> = (props) => {
  const { children, useOpacity, ...rest } = props;
  return (
    <Cell className={styles.td} {...rest}>
      {children}
      {useOpacity ? (
        <div className={styles.overflowOpacity} />
      ) : null}
    </Cell>
  );
};