import * as React from "react";
import styles from "../table.module.scss";

export type ThProps = React.HTMLAttributes<HTMLTableRowElement>;

export const Tr: React.FC<ThProps> = (props) => {
  return <div className={styles.tr} {...props} />;
};