import * as React from "react";
import classNames from "classnames";
import styles from "../table.module.scss";

export type CellProps = React.HTMLAttributes<HTMLDivElement> & {
  width: number;
  left?: number;
};

export const Cell: React.FC<CellProps> = (props) => {
  const { className, left, width, ...rest } = props;
  return (
    <div
      className={classNames(styles.cell, props.className)}
      {...rest}
      style={{
        /*left: `${left}px`,*/
        /*width: `${width}px`*/
      }}
    />
  );
};
