import * as React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Opacity01 from './tables/Opacity01';
import Opacity02 from "./tables/Opacity02";
import Opacity03 from "./buttons/Opacity03";
import Opacity04 from "./buttons/Opacity04";

export default function App() {
  return (
    <Router>
      <nav className="nav-horizontal">
        <Link to="/opacity-01">Table (transp.)</Link>
        <Link to="/opacity-02">Table (no transp.)</Link>
        <Link to="/opacity-03">Transparent Buttons</Link>
        <Link to="/opacity-04">Opaque Buttons</Link>
      </nav>
      <Switch>
        <Route path="/opacity-01">
          <Opacity01 />
        </Route>
        <Route path="/opacity-02">
          <Opacity02 />
        </Route>
        <Route path="/opacity-03">
          <Opacity03 />
        </Route>
        <Route path="/opacity-04">
          <Opacity04 />
        </Route>
        <Route path="/">
          <Opacity01 />
        </Route>
      </Switch>
    </Router>
  );
}
