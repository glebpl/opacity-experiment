import * as React from "react";
import {Table} from "./Table";
import VerticalDelimiter from '../style/VerticalDelimiter';
import { Controls } from "./Controls";

const initialWidth = (window.innerWidth - 5) * 0.75;

export default function Opacity01() {
  const [columnsNumber, setColumnsNumber] = React.useState(15);
  const handleChangeColumnsNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setColumnsNumber(Number(e.target.value));
  };

  const [rowsNumber, setRowsNumber] = React.useState(50);
  const handleChangeRowsNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRowsNumber(Number(e.target.value));
  };

  const [tableWidth, setTableWidth] = React.useState(initialWidth);
  const handleResizeMove = (left: number) => {
    setTableWidth(left);
  };

  return (
    <div className="App" style={{
      width: `${tableWidth}px`
    }}>
      <Controls
        columnsNumber={columnsNumber}
        handleChangeColumnsNumber={handleChangeColumnsNumber}
        rowsNumber={rowsNumber}
        handleChangeRowsNumber={handleChangeRowsNumber}
      />
      <Table
        useOpacity={false}
        columnsNumber={columnsNumber}
        rowsNumber={rowsNumber}
        width={tableWidth}
      />
      <VerticalDelimiter onDragMove={handleResizeMove} initialLeft={initialWidth}/>
    </div>
  );
}
