import * as React from "react";

type Callback = (e: React.ChangeEvent<HTMLInputElement>) => void;

type Props = {
  columnsNumber: number;
  handleChangeColumnsNumber: Callback;
  rowsNumber: number;
  handleChangeRowsNumber: Callback;
  // useOpacity?: boolean;
};

export const Controls: React.FC<Props> = (props) => {
  const {
    columnsNumber,
    handleChangeColumnsNumber,
    rowsNumber,
    handleChangeRowsNumber
  } = props;
  return (
    <div className="controls">
      <div>
        <div>
          <div>
            Columns
            <input
              type="number"
              value={columnsNumber}
              onChange={handleChangeColumnsNumber}
            />
          </div>
          <div>
            Rows
            <input
              type="number"
              value={rowsNumber}
              onChange={handleChangeRowsNumber}
              step={10}
            />
          </div>
          {/*<div>*/}
          {/*  <label>*/}
          {/*    Use opacity*/}
          {/*    <input*/}
          {/*      type="checkbox"*/}
          {/*      checked={useOpacity}*/}
          {/*      onChange={handleChangeUseOpacity}*/}
          {/*    />*/}
          {/*  </label>*/}
          {/*</div>*/}
        </div>
      </div>
    </div>
  );
};