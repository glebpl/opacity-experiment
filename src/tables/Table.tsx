import * as React from "react";
import { LoremIpsum } from "lorem-ipsum";
import styles from "../table.module.scss";
import { Th } from "../style/Th";
import { Td } from "../style/Td";
import { Col, createColumns } from "../columns";
import {Tr} from '../style/Tr';

type Props = React.HTMLAttributes<HTMLDivElement> & {
  columnsNumber: number;
  resizeOnly?: number;
  width: number;
  rowsNumber?: number;
  useOpacity: boolean;
};

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

const createRows = (columns: Col[], total: number) => {
  const rows = [];
  for (let i = 0; i < total; ++i) {
    const data: Record<string, string> = {};
    columns.forEach((col) => {
      data[col.id] = `${i}: ${lorem.generateWords(10)}`;
    });
    rows.push({ data });
  }
  return rows;
};

export const Table: React.FC<Props> = (props) => {
  const { columnsNumber, rowsNumber = 10, useOpacity, width, resizeOnly = 0 } = props;
  const columns = React.useMemo(() => {
    return createColumns(columnsNumber);
  }, [columnsNumber]);
  const rows = React.useMemo(() => {
    return createRows(columns, rowsNumber);
  }, [columns, rowsNumber]);
  let sumWidth = 0;

  const calcWidth = (col: Col, index: number): number => {
    if(resizeOnly >= 0 && resizeOnly < columns.length) {
      if (index === resizeOnly) {
        return width - (50 * (columns.length - 1));
      } else {
        return 50;
      }
    } else {
      return width / columns.length;
    }
  }

  return (
    <div className={styles.table}>
      <div className={styles.thead}>
        <div className={styles.tr}>
          {columns.map((col, i) => {
            const w = calcWidth(col, i);
            return (
              <Th
                key={col.id}
                /*left={(sumWidth += i > 0 ? w : 0)}*/
                width={w}
              >
                {col.name}
              </Th>
            );
          })}
        </div>
      </div>
      <div className={styles.tbody}>
        {rows.map((row, rowIndex) => {
          let w = 0;
          return (
            <Tr key={rowIndex}>
              {columns.map((col, i) => {
                const cw = calcWidth(col, i);
                return (
                  <Td
                    key={i}
                    /*left={(w += i > 0 ? cw : 0)}*/
                    width={cw}
                    useOpacity={useOpacity}
                  >
                    {row.data[col.id]}
                  </Td>
                );
              })}
            </Tr>
          );
        })}
      </div>
    </div>
  );
};
