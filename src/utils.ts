export const getViewport = () => ({
  w: document.body.clientWidth
  , h: document.body.scrollWidth
});

export const cssTranslate = ({x, y}: {x: number; y: number}) => `translate3d(${x}px, ${y}px, 0)`;

export const toggleClass = (el: HTMLElement, className: string, on: boolean) => {
  el.classList[on ? 'add' : 'remove'](className);
};

export const throttle = (func: Function, ms: number) => {
  let isThrottled = false,
    savedArgs: any[] | null,
    savedThis: any;

  function wrapper(this: any, ...args: any[]) {
    if (isThrottled) { // (2)
      savedArgs = args;
      savedThis = this;
      return;
    }

    func.apply(this, arguments); // (1)

    isThrottled = true;

    setTimeout(function() {
      isThrottled = false; // (3)
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = savedThis = null;
      }
    }, ms);
  }

  return wrapper;
};
