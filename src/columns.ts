export interface Col {
  id: string;
  name: string;
  width: number;
}

export const createColumns = (total: number): Col[] => {
  const columns: Col[] = [];
  for (let i = 0; i < total; ++i) {
    columns.push({
      id: `col${i}`,
      name: `Col ${i}`,
      width: 80
    });
  }
  return columns;
};
