import * as React from "react";

type Props = React.AllHTMLAttributes<HTMLInputElement>;

export const Controls: React.FC<Props> = (props) => (
  <div className="controls">
    <div>
      Quantity
      <input
        type="number"
        step={5}
        {...props}
      />
    </div>
  </div>
);