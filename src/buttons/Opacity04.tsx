import * as React from "react";
import "./buttons-experiment.scss";
import {lorem2} from './utils';
import {Controls} from './Controls';
import {ButtonsOverText} from './ButtonsOverText';
import {TransparentBtn} from '../style/TransparentBtn';
import {PrimaryBtn} from '../style/PrimaryBtn';

export default function Opacity04() {
  const [buttonsNumber, setButtonsNumber] = React.useState(40);
  const handleChangeButtonsNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setButtonsNumber(Number(e.target.value));
  };

  const buttons = React.useMemo(() => {
    return new Array(buttonsNumber).fill('').map(() => lorem2.generateWords(2));
  }, [buttonsNumber]);

  return (
    <div className="App">
      <Controls
        value={buttonsNumber}
        onChange={handleChangeButtonsNumber}
      />
      <ButtonsOverText>
        {buttons.map((txt, i) => (
          <PrimaryBtn key={i}>{txt}</PrimaryBtn>
        ))}
      </ButtonsOverText>
    </div>
  );
}
