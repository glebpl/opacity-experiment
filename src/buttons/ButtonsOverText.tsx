import * as React from "react";
import {lorem1} from './utils';

type Props = React.AllHTMLAttributes<HTMLInputElement>;

export const ButtonsOverText: React.FC<Props> = (props) => {
  const paragraphs = React.useMemo(() => {
    return new Array(20).fill('').map(() => lorem1.generateSentences(1));
  }, []);
  return (
    <div className="buttons-underlay">
      {paragraphs.map((txt, i) => (
        <p key={i}>{txt}</p>
      ))}
      <div className="buttons-overlay">
        {props.children}
      </div>
    </div>
  )
};
