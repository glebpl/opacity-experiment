import {LoremIpsum} from 'lorem-ipsum';

export const lorem1 = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 3
  },
  wordsPerSentence: {
    max: 100,
    min: 20
  }
});

export const lorem2 =  new LoremIpsum({
  wordsPerSentence: {
    max: 4,
    min: 1
  }
});